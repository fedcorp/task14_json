package com.fedcorp.planesLibrary;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Chars {

    @JsonProperty("type")
    private String planeType;
    @JsonProperty("seating")
    private int sitsNum;
    private int ammo = 0;
    private String radar;

    public String getPlaneType() {
        return planeType;
    }

    public void setPlaneType(String planeType) {
        this.planeType = planeType;
    }

    public int getSitsNum() {
        return sitsNum;
    }

    public void setSitsNum(int sitsNum) {
        this.sitsNum = sitsNum;
    }

    public int getAmmo() {
        return ammo;
    }

    public void setAmmo(int ammo) {
        this.ammo = ammo;
    }

    public String getRadar() {
        return radar;
    }

    public void setRadar(String radar) {
        this.radar = radar;
    }

    @Override
    public String toString() {
        return  "type - " + planeType +
                ", seating - " + sitsNum +
                ", ammo - " + ammo +
                ", radar - " + radar;
    }
}
