package com.fedcorp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedcorp.planesLibrary.Plane;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JacksonParser {
    public static List<Plane> parseJson(File file){
        ObjectMapper mapper = new ObjectMapper();
        Plane[] planes = null;
        try {
            planes = mapper.readValue(file, Plane[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(planes!=null) {
            return new ArrayList<Plane>(Arrays.asList(planes));
        }else{
            return null;
        }
    }
}
