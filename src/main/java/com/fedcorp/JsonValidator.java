package com.fedcorp;



import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.examples.Utils;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import java.io.File;

public class JsonValidator {

    public static void validate(String path){
        try {
            final JsonNode fstabSchema = Utils.loadResource("\\src\\main\\resources\\json\\planesSchema.json");
            final JsonNode json = Utils.loadResource("\\src\\main\\resources\\json\\planes.json");
            final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
            final JsonSchema schema = factory.getJsonSchema(fstabSchema);

            ProcessingReport report;

            report = schema.validate(json);
            System.out.println(report);

        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
